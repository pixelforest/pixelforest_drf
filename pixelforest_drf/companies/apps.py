from django.apps import AppConfig


class CompaniesConfig(AppConfig):
    name = 'pixelforest_drf.companies'
