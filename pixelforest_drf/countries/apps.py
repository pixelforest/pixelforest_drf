from django.apps import AppConfig


class countriesConfig(AppConfig):
    name = 'pixelforest_drf.countries'
