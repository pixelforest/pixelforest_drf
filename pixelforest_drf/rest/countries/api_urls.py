# Imports ##############################################################################################################
from rest_framework import routers

from .api_views import RegionViewSet, SubRegionViewSet, CountriesViewSet

router = routers.DefaultRouter()
router.register(r'countries/regions', RegionViewSet, 'region')
router.register(r'countries/sub_regions', SubRegionViewSet, 'sub_region')
router.register(r'countries/countries', CountriesViewSet, 'countries')

