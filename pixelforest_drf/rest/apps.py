# Imports ##############################################################################################################
from django.apps import AppConfig


class RestConfig(AppConfig):
    name = 'pixelforest_drf.rest'