# Imports #############################################################################################################
from django.contrib.auth import get_user_model
from django.core.exceptions import ImproperlyConfigured

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import ListModelMixin

from pixelforest_drf.users.models import PFUser
from .serializers import UsersSerializer, UsersMeSerializer
from ..permissions import FullDjangoModelPermissions

User = get_user_model()

if User is not PFUser:
    raise ImproperlyConfigured("Pf User is not the User model")


class UsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UsersSerializer
    permission_classes = [FullDjangoModelPermissions]


class MeViewSet(viewsets.ModelViewSet, ListModelMixin):
    serializer_class = UsersMeSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ['get']

    def get_queryset(self):
        user = self.request.user
        queryset = User.objects.filter(pk=user.pk)
        return queryset
