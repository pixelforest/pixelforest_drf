# Imports ##############################################################################################################
from rest_framework import serializers

from pixelforest_drf.users.models import PFUser
from pixelforest_drf.companies.models import Service, Subsidiary, Company
from pixelforest_drf.countries.models import Country, SubRegion
from ..companies.serializers import CompanyGroupSerializer
from ..countries.serializers import RegionSerializer


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = PFUser
        fields = '__all__'


class SubRegionMeSerializer(serializers.HyperlinkedModelSerializer):
    region = RegionSerializer()

    class Meta:
        model = SubRegion
        fields = ['name', 'is_active', 'region']


class CountryMeSerializer(serializers.HyperlinkedModelSerializer):
    sub_region = SubRegionMeSerializer()

    class Meta:
        model = Country
        fields = ['name', 'iso_alpha_2', 'iso_alpha_3', 'iso_num', 'flag', 'is_active', 'sub_region']


class CompanyMeSerializer(serializers.HyperlinkedModelSerializer):
    company_group = CompanyGroupSerializer()

    class Meta:
        model = Company
        fields = ['name', 'logo', 'is_active', 'company_group']


class SubsidiaryMeSerializer(serializers.HyperlinkedModelSerializer):
    company = CompanyMeSerializer()

    class Meta:
        model = Subsidiary
        fields = ['name', 'logo', 'is_active', 'company']


class ServiceMeSerializer(serializers.HyperlinkedModelSerializer):
    subsidiary = SubsidiaryMeSerializer()

    class Meta:
        model = Service
        fields = ['name', 'logo', 'is_active', 'subsidiary']


class UsersMeSerializer(serializers.HyperlinkedModelSerializer):
    service = ServiceMeSerializer()
    country = CountryMeSerializer()

    class Meta:
        model = PFUser
        fields = ['email', 'first_name', 'last_name', 'is_staff', 'phone', 'is_active', 'service', "country"]
