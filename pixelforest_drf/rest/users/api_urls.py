# Imports ##############################################################################################################
from rest_framework import routers

from .api_views import UsersViewSet, MeViewSet

router = routers.DefaultRouter()
router.register(r'users/me', MeViewSet, 'me')
router.register(r'users', UsersViewSet, 'user')
