# Imports ##############################################################################################################
from rest_framework import routers

from .api_views import CompanyGroupViewSet, CompanyViewSet, SubsidiaryViewSet, ServiceViewSet


router = routers.DefaultRouter()
router.register(r'companies/companies_group', CompanyGroupViewSet, 'company_group')
router.register(r'companies/companies', CompanyViewSet, 'company')
router.register(r'companies/subsidiaries', SubsidiaryViewSet, 'subsidiary')
router.register(r'companies/services', ServiceViewSet, 'service')
