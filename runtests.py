#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner


def run_tests(*test_args):
    # Setup the django instance with the testing settings
    os.environ['DJANGO_SETTINGS_MODULE'] = 'tests.settings'
    django.setup()
    # Get the test runner
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    # Run the tests
    failures = test_runner.run_tests([])
    sys.exit(bool(failures))


if __name__ == '__main__':
    run_tests()
