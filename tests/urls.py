# Imports ##############################################################################################################
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include

from rest_framework import routers

from pixelforest_drf.rest.countries.api_urls import router as router_countries
from pixelforest_drf.rest.users.api_urls import router as router_users
from pixelforest_drf.rest.companies.api_urls import router as router_companies


router = routers.DefaultRouter()
router.registry.extend(router_countries.registry)
router.registry.extend(router_users.registry)
router.registry.extend(router_companies.registry)

urlpatterns = [
    path('api/', include((router.urls, "pixelforest_drf"))),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
