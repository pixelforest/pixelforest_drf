# Import ###############################################################################################################
from django.test import TestCase

from ..models import IsActiveModel


# Tests ################################################################################################################

class ActiveQuerysetModelTest(TestCase):
    def setUp(self):
        self.a = IsActiveModel.objects.create(name="A", is_active=False)
        self.b = IsActiveModel.objects.create(name="B", is_active=False)
        self.c = IsActiveModel.objects.create(name="C", is_active=True)
        self.d = IsActiveModel.objects.create(name="D", is_active=False)
        self.e = IsActiveModel.objects.create(name="E", is_active=True)
        self.f = IsActiveModel.objects.create(name="F", is_active=True)

    def test_active_queryset(self):
        self.assertQuerysetEqual(IsActiveModel.objects.active(),
                                 [repr(self.c), repr(self.e), repr(self.f)],
                                 ordered=False)

    def test_inactive_queryset(self):
        self.assertQuerysetEqual(IsActiveModel.objects.inactive(),
                                 [repr(self.a), repr(self.b), repr(self.d)],
                                 ordered=False)
