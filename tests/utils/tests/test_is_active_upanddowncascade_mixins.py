# Imports ##############################################################################################################
from django.test import TestCase

from ..models import Level1UpAndDown, Level2UpAndDown, LevelAUpAndDown, LevelBUpAndDown


# Private Mixin UpAndDownIsActive TestCase #############################################################################
class PrivateModelUpAndDownIsActiveMixinTest(TestCase):

    def test_parent_pass_active(self):
        """
        Related objects is_active field becomes True if object is created with is_active=True (cascade of 1 object)
        """
        self.lvl2 = Level2UpAndDown.objects.create(is_active=False)

        self.lvl1 = Level1UpAndDown.objects.create(is_active=True, level2=self.lvl2)

        self.assertTrue(self.lvl1.is_active)
        self.assertTrue(self.lvl2.is_active)

    def test_children_pass_inactive(self):
        """
        Related objects is_active field becomes False if parent object is modified with is_active=False (1 object)
        """

        # Create one lvl2 object named "lvl2" set to True.
        self.lvl2 = Level2UpAndDown.objects.create(is_active=True)

        # Create lvl1 object that have a foreignkey to the lvl2 object, those objects are set to True.
        self.lvl1 = Level1UpAndDown.objects.create(is_active=True, level2=self.lvl2)

        Level2UpAndDown.objects.filter(pk=self.lvl2.pk).update(is_active=False)

        # Get the objects from the database
        inst = Level2UpAndDown.objects.get(pk=self.lvl2.pk)
        inst1 = Level1UpAndDown.objects.get(pk=self.lvl1.pk)

        # Test is_active field value.
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)


# Public Mixin UpAndDownIsActive TestCase ##############################################################################

class PublicModelUpAndDownIsActiveMixinTest(TestCase):
    """
    Be sure  that public class inherit from private and ads is_active field to the Models.
    +-----------+    +----------+
    | Level A   +--->+ Level B  |
    +-----------+    +----------+
    """
    def test_public_downward(self):
        self.lvlb = LevelBUpAndDown.objects.create(name="lvlb", is_active=True)
        self.lvla = LevelAUpAndDown.objects.create(name="lvla", is_active=True, levelb=self.lvlb)

        LevelBUpAndDown.objects.filter(name="lvlb").update(is_active=False)

        inst = LevelAUpAndDown.objects.get(name="lvla")
        self.assertFalse(inst.is_active)

    def test_public_upward(self):
        self.lvlb = LevelBUpAndDown.objects.create(name="lvlb", is_active=False)
        self.lvla = LevelAUpAndDown.objects.create(name="lvla", is_active=False, levelb=self.lvlb)

        self.lvla.is_active = True
        self.lvla.save()

        self.assertTrue(self.lvlb.is_active)