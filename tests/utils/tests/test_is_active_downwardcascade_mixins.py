# Imports ##############################################################################################################
from django.test import TestCase
from django.core.exceptions import FieldDoesNotExist

from ..models import (
    LevelAdown, LevelBdown, Level1down, Level2down, Level3down, Level4down, Level5down, Level6down, Level7down,
    Level8down, IsActiveWithoutMixin, IsActiveWithMixin,
)


# Private Mixin DownIsActive TestCase ##################################################################################

class PrivateModelDownIsActiveMixinTest1level(TestCase):
    """
    Test for creation object, We need one level model with is_active field and Mixin.
    +--------+
    + level1 +
    +--------+
    """
    def test_creation(self):
        """ Is_active field objects stays False or True at creation """
        # create 2 objects : one set to False, second set to True
        self.lvl1false = Level1down.objects.create(name="lvl1", is_active=False)
        self.lvl1true = Level1down.objects.create(name="lvl1", is_active=True)

        # These 2 is_active field objects must be identical to their creation
        self.assertFalse(self.lvl1false.is_active)
        self.assertTrue(self.lvl1true.is_active)

    def test_update(self):
        """ Is_active field objects becomes False if is_active is update to False"""
        # create 2 objects set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True)
        self.lvl1bis = Level1down.objects.create(name="lvl1bis", is_active=True)

        # Test queryset call:
        with self.assertNumQueries(2):
            # update is_active to False
            Level1down.objects.filter(name__in=["lvl1", "lvl1bis"]).update(is_active=False)

        # Get the objects from database
        inst = Level1down.objects.get(name="lvl1")
        inst1 = Level1down.objects.get(name="lvl1bis")

        # Test is_active field value
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)

    def test_save(self):
        """ Test that Save implementation is working as update"""
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True)

        # Get the object from database
        inst = Level1down.objects.get(name="lvl1")

        # switch is_active field instance to False
        inst.is_active = False

        # save instance
        inst.save()

        # Test is_active field value
        self.assertFalse(inst.is_active)


class PrivateModelDownIsActiveMixinTest2levels(TestCase):
    """
    Test that update object impact is_active Fields Children, We need two level Models with is_active field
    and mixin.
    +---+---+    +---+---+
    +level 1+--->+level 2+
    +-------+    +-------+
     """

    def test_children_objects_update_stay_active(self):
        """
        Related objects is_active field stays True if parents object is modified with is_active=True
        """
        # Create one lvl2 object named "lvl2" set to True.
        self.lvl2 = Level2down.objects.create(name="lvl2", is_active=True)

        # Create lvl1 objects that have a foreignkey to the lvl2 object, these is_active field objects are set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True, level2=self.lvl2)
        self.lvl1bis = Level1down.objects.create(name="lvl1bis", is_active=True, level2=self.lvl2)
        self.lvl1ter = Level1down.objects.create(name="lvl1ter", is_active=True, level2=self.lvl2)

        # Test number of queryset call
        with self.assertNumQueries(1):
            # Launch an update to lvl2 object (lvl2) , switch is_active Field to True.
            # That action should not affect the database objects.
            Level2down.objects.filter(name="lvl2").update(is_active=True)

        # Get the objects from the database
        inst = Level2down.objects.get(name="lvl2")
        inst1 = Level1down.objects.get(name="lvl1")
        inst2 = Level1down.objects.get(name="lvl1bis")
        inst3 = Level1down.objects.get(name="lvl1ter")

        # Test is_active field value.
        self.assertTrue(inst.is_active)
        self.assertTrue(inst1.is_active)
        self.assertTrue(inst2.is_active)
        self.assertTrue(inst3.is_active)

    def test_children_object_update_pass_inactive(self):
        """
        Related objects is_active field becomes False if parent object is modified with is_active=False (1 object)
        """

        # Create one lvl2 object named "lvl2" set to True.
        self.lvl2 = Level2down.objects.create(name="lvl2", is_active=True)

        # Create lvl1 objects that have a foreignkey to the lvl2 object, those objects are set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True, level2=self.lvl2)
        self.lvl1bis = Level1down.objects.create(name="lvl1bis", is_active=True, level2=self.lvl2)
        self.lvl1ter = Level1down.objects.create(name="lvl1ter", is_active=True, level2=self.lvl2)

        # Test number of queryset call
        with self.assertNumQueries(4):
            # Launch an update to lvl2 object (lvl2) , switch is_active Field to False.
            # That action must switch all is_active field lvl1 objects from True to False
            Level2down.objects.filter(name="lvl2").update(is_active=False)

        # Get the objects from the database
        inst = Level2down.objects.get(name="lvl2")
        inst1 = Level1down.objects.get(name="lvl1")
        inst2 = Level1down.objects.get(name="lvl1bis")
        inst3 = Level1down.objects.get(name="lvl1ter")

        # Test is_active field value.
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)
        self.assertFalse(inst2.is_active)
        self.assertFalse(inst3.is_active)

    def test_children_objects_update_pass_inactive(self):
        """
        Related objects is_active field becomes False if parent object is modified with is_active=False (2 object)
        """

        # Create lvl2 objects, those objects are set to True.
        self.lvl2 = Level2down.objects.create(name="lvl2", is_active=True)
        self.lvl2bis = Level2down.objects.create(name="lvl2bis", is_active=True)

        # Create lvl1 objects that have a foreignkey to lvl 2objects, those objects are set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True, level2=self.lvl2)
        self.lvl1bis = Level1down.objects.create(name="lvl1bis", is_active=True, level2=self.lvl2bis)

        # Test number of queryset call
        with self.assertNumQueries(4):
            # Launch an update to two lvl2 objects (lvl2, lvl2bis) and switch is_active Field to False.
            # That action must switch all our is_active field lvl1 and lvl2 objects from True to False
            Level2down.objects.filter(name__in=["lvl2", "lvl2bis"]).update(is_active=False)

        # Get the objects from the database
        inst = Level1down.objects.get(name="lvl1")
        inst1 = Level1down.objects.get(name="lvl1bis")
        inst2 = Level2down.objects.get(name="lvl2")
        inst3 = Level2down.objects.get(name="lvl2bis")

        # Test if is_active field objects becomes False.
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)
        self.assertFalse(inst2.is_active)
        self.assertFalse(inst3.is_active)

    def test_update_with_wrong_kwargs(self):
        """
        Objects is_active field stays as is if an object is updated with any wrong kwargs
        """
        # Create lvl2 object named "lvl2" set to True.
        self.lvl2 = Level2down.objects.create(name="lvl2", is_active=True)

        # Create lvl1 object that have a foreignkey to the lvl2 object, is_active field set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True, level2=self.lvl2)

        # Catch the Field Error:
        with self.assertRaises(FieldDoesNotExist):
            # Launch an update to lvl2 object with wrong kwargs,
            # That action should not affect the database objects.
            Level2down.objects.filter(name="lvl3").filter(is_active=True).update(is_active=False, random="HelloWorld")

        # Get the lvl1 and lvl2 objects from the database
        inst1 = Level2down.objects.get(name="lvl2")
        inst2 = Level1down.objects.get(name="lvl1")

        # Test if the objects is_active Field stays True
        self.assertTrue(inst1.is_active)
        self.assertTrue(inst2.is_active)


class PrivateModelDownIsActiveMixinTestWithNoMixinModels(TestCase):
    """
    Test Mixin compatibility with no Mixin models.
      +----------+    +-----------+
      + no Mixin +--->+   Mixin   |
      +----------+    +----------+
    """
    def test_children_without_mixin_with_is_active_field(self):
        """
        Even if the Children model don't have the mixin, if a parent Model object is_active field is turn to
        False and have the mixin, all the related children is_active field is switch to False.
        """

        # Create objects in database
        self.mixin = IsActiveWithMixin.objects.create(name="mixin", is_active=True)
        self.nomixin = IsActiveWithoutMixin.objects.create(name="nomixin", is_active=True, withmixin=self.mixin)

        # Launch an update to mixin object,
        # That action must switch all is_is_active field object "IsActiveWithoutMixin" related to lvl1 from True,
        # to False
        IsActiveWithMixin.objects.filter(name="mixin").update(is_active=False)

        # Get the object from database
        inst = IsActiveWithMixin.objects.get(pk=self.mixin.pk)
        inst1 = IsActiveWithoutMixin.objects.get(pk=self.nomixin.pk)

        # Verify mixin is_active field is False, and IsaActiveWithoutMixin object is_active field becomes False.
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)


class PrivateModelDownIsActiveMixinTestCascade(TestCase):
    """
    Related objects is_active field becomes False if parent object is modified with is_active=False
    3 Models level:
    +---+---+    +---+---+    +---+---+
    +level 1+--->+level 2+--->+level 3|
    +---+---+    +-------+    +-------+
    """

    def test_update_cascade_is_active_children(self):

        # Create lvl3 object named "lvl3" set to True.
        self.lvl3 = Level3down.objects.create(name="lvl3", is_active=True)

        # Create lvl2 objects that have a foreignkey to the lvl3 object, those objects are set to True.
        self.lvl2 = Level2down.objects.create(name="lvl2", is_active=True, level3=self.lvl3)
        self.lvl2bis = Level2down.objects.create(name="lvl2bis", is_active=True, level3=self.lvl3)
        self.lvl2ter = Level2down.objects.create(name="lvl2ter", is_active=True, level3=self.lvl3)

        # Create lvl1 objects that have a foreignkey to one of the lvl 2object, those objects are set to True.
        self.lvl1 = Level1down.objects.create(name="lvl1", is_active=True, level2=self.lvl2)
        self.lvl1bis = Level1down.objects.create(name="lvl1bis", is_active=True, level2=self.lvl2bis)
        self.lvl1ter = Level1down.objects.create(name="lvl1ter", is_active=True, level2=self.lvl2bis)

        # Test number of queryset call
        with self.assertNumQueries(6):
            # Launch an update to lvl3 object (self.lvl3) , switch is_active Field to False.
            # That action must switch all our is_active field lvl1 and lvl2 objects from True to False
            Level3down.objects.filter(name="lvl3").filter(is_active=True).update(is_active=False)

        # Get the lvl1 objects from the database
        inst = Level1down.objects.get(name="lvl1")
        inst1 = Level1down.objects.get(name="lvl1bis")
        inst2 = Level1down.objects.get(name="lvl1ter")

        # Test is_active value
        self.assertFalse(inst.is_active)
        self.assertFalse(inst1.is_active)
        self.assertFalse(inst2.is_active)


class PrivateModelDownIsActiveMixinTestCascadeWithoutIsActiveField(TestCase):
    """
    Update is_active to False does not modify is_active field if there is a model without is_active field between
    them. Example :
      +-----------+    +----------+    +-----------+
      | is_active +--->+ no field +--->+ is_active |
      +-----------+    +----------+    +-----------+
    """

    def test_cascade_without_is_active_field_django_models(self):
        # Intermediate model (Level6) using models.Model
        self.lvl8 = Level8down.objects.create(name="lvl8", is_active=True)
        self.lvl6 = Level6down.objects.create(name="lvl6", level8=self.lvl8)
        self.lvl5 = Level5down.objects.create(name="lvl5", level6=self.lvl6, is_active=True)

        # Launch an update to lvl8 object, that action should switch lvl8 object is_active field to False but lvl5
        # is_active field object should stay True.
        Level8down.objects.filter(name="lvl8").update(is_active=False)

        # Get the object from database
        inst = Level5down.objects.get(name="lvl5")

        # Verify that lvl5 object stay active
        self.assertTrue(inst.is_active)

    def test_cascade_without_is_active_field_mixin_models(self):
        # Intermediate model (Level4) using DownIsActiveMixin
        self.lvl7 = Level7down.objects.create(name="lvl7", is_active=True)
        self.lvl4 = Level4down.objects.create(name="lvl4", level7=self.lvl7)
        self.lvl3 = Level3down.objects.create(name="lvl3", level4=self.lvl4, is_active=True)

        # Launch an update to lvl7 object, that action should switch lvl7 object is_active field to False but lvl3
        # is_active field object should stay True.
        Level7down.objects.filter(name="lvl7").update(is_active=False)

        # Get the object from database
        inst = Level3down.objects.get(name="lvl3")

        # Verify that lvl3 object stay active
        self.assertTrue(inst.is_active)


# Public Mixin DownIsActive TestCase ###################################################################################

class PublicModelDownIsActiveMixinTest(TestCase):
    """
    Be sure  that public class inherit from private and ads is_active field to the Models.
    +-----------+    +----------+
    | Level A   +--->+ Level B  |
    +-----------+    +----------+
    """

    def test_public_downwardmixin(self):
        self.lvlb = LevelBdown.objects.create(name="lvlb", is_active=True)
        self.lvla = LevelAdown.objects.create(name="lvla", is_active=True, levelb=self.lvlb)

        LevelBdown.objects.filter(name="lvlb").update(is_active=False)

        inst = LevelAdown.objects.get(name="lvla")
        self.assertFalse(inst.is_active)
