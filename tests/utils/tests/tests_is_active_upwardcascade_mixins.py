# Imports ##############################################################################################################
from django.test import TestCase

from ..models import Level1Up, Level2Up, Level3Up, Level4Up, Level5Up, Level6Up, Level7Up, Level8Up, LevelAUp, LevelBUp


# Private Mixin IsUpActive TestCase ####################################################################################

class PrivateModelUpIsActiveMixinTest(TestCase):
    """
    Only ForeignKey relations are tested
    Mixin Test UpIsActiveField
    All Models Have is_active Fields Except Level4 and Level6.
                 +-------+    +-------+    +-------+
        +------->+level 5+--->+level 6+--->+level 8|
        |        +-------+    +-------+    +-------+
        |
    +---+---+    +-------+    +-------+
    |level 1+--->+level 2+--->+level 3|
    +-------+    +-------+    +---+---+
                    |             |
                    |             |
                    |             v
                    |         +---+---+    +-------+
                    +-------->+level 4+--->+level 7|
                              +-------+    +-------+

    Save count is included in some case for optimisation testing
    """


class PrivateModelUpIsActiveMixinTest2levels(TestCase):
    """
        Test that update or create object impact is_active Fields Parents, We need two level Models with is_active field
        and mixin.
        +---+---+    +---+---+
        +level 1+--->+level 2+
        +-------+    +-------+
    """

    def test_create_parent_lvl1_stay_inactive(self):
        """ Related objects is_active field stays False if object is created with is_active=False"""
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=False, level2=self.lvl2)

        # Test is_active value
        self.assertFalse(self.lvl1.is_active)
        self.assertFalse(self.lvl2.is_active)

        # Count save call
        self.assertEqual(self.lvl1.save_count, 1)
        self.assertEqual(self.lvl2.save_count, 1)

    def test_create_parent_pass_active(self):
        """
        Related objects is_active field becomes True if object is created with is_active=True (cascade of 1 object)
        """
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False)

        # Create an object set to True that have a foreignkey affect is_active field foreignkey value to True
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=True, level2=self.lvl2)

        # Test is_active value
        self.assertTrue(self.lvl1.is_active)
        self.assertTrue(self.lvl2.is_active)

        # count save call
        self.assertEqual(self.lvl2.save_count, 2)

    def test_update_is_active_True(self):
        """ Change is_active from False to True in current object"""
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=False, level2=self.lvl2)

        self.lvl1.is_active = True
        self.lvl1.save()

        self.assertTrue(self.lvl1.is_active)
        self.assertTrue(self.lvl2.is_active)

    def test_update_is_active_False(self):
        """ Change is_active from True to False in current object"""
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=True)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=True, level2=self.lvl2)

        self.lvl1.is_active = False
        self.lvl1.save()

        self.assertFalse(self.lvl1.is_active)
        self.assertTrue(self.lvl2.is_active)

    def test_update_new_related_instances_is_active(self):
        """ Change the upward related object (self.is_active = True) """
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False)
        self.lvl2bis = Level2Up.objects.create(name="lvl2bis", is_active=False)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=False, level2=self.lvl2)

        # Change the related foreignkey object
        self.lvl1.level2 = self.lvl2bis
        self.lvl1.is_active = True
        self.lvl1.save()

        # Test is_active field value
        self.assertTrue(self.lvl2bis.is_active)

        # Count save
        self.assertEqual(self.lvl1.save_count, 2)
        self.assertEqual(self.lvl2bis.save_count, 2)

    def test_update_change_new_related_instances_is_active_when_self_is_active_false(self):
        """Change the upward related object (self.is_active = False)"""
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False)
        self.lvl2bis = Level2Up.objects.create(name="lvl2bis", is_active=False)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=False, level2=self.lvl2)

        # Change the related foreignkey object
        self.lvl1.level2 = self.lvl2bis
        self.lvl1.save()

        self.assertFalse(self.lvl2bis.is_active)

        self.assertEqual(self.lvl1.save_count, 2)
        self.assertEqual(self.lvl2bis.save_count, 1)


class PrivateModelUpIsActiveMixinTestCascade(TestCase):
    """
    Related objects is_active field becomes True if children object is modified with is_active=True
    3 Models level:
    +---+---+    +---+---+    +---+---+
    +level 1+--->+level 2+--->+level 3|
    +---+---+    +-------+    +-------+
    """
    def test_create_cascade_is_active_parents(self):
        """
        Related objects is_active field becomes True if object is created with is_active=True (cascade of 2 object)
        """
        self.lvl3 = Level3Up.objects.create(name="lvl3", is_active=False)
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False, level3=self.lvl3)

        # Create an object set to True that have a foreignkey affect is_active field foreignkey value to True
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=True, level2=self.lvl2)

        # Test is_active value
        self.assertTrue(self.lvl3.is_active)

        # count save call
        self.assertEqual(self.lvl3.save_count, 2)

    def test_update_is_active_cascade(self):
        """Update object with is_active=True and sets the is_active field of all related parent objects to True"""
        self.lvl3 = Level3Up.objects.create(name="lvl3", is_active=False)
        self.lvl2 = Level2Up.objects.create(name="lvl2", is_active=False, level3=self.lvl3)
        self.lvl1 = Level1Up.objects.create(name="lvl1", is_active=False, level2=self.lvl2)

        self.lvl1.is_active = True
        self.lvl1.save()

        self.assertTrue(self.lvl3.is_active)


class PrivateModelUpIsActiveMixinTestWithoutIsActiveField(TestCase):
    """
    Save is_active as True does not modify is_active field if there is a model without is_active field between
    them. Example :
      +-----------+    +----------+    +-----------+
      | is_active +--->+ no field +--->+ is_active |
      +-----------+    +----------+    +-----------+
    """

    def test_cascade_without_is_active_field_using_django_models(self):
        """ Intermediate model using models.Model """
        self.lvl8 = Level8Up.objects.create(name="lvl8", is_active=False)
        self.lvl6 = Level6Up.objects.create(name="lvl6", level8=self.lvl8)
        self.lvl5 = Level5Up.objects.create(name="lvl5", level6=self.lvl6, is_active=True)

        self.assertFalse(self.lvl8.is_active)
        self.assertEqual(self.lvl8.save_count, 1)

    def test_cascade_without_is_active_field_using_mixin(self):
        """ Intermediate model using UpIsActiveMixin """
        self.lvl7 = Level7Up.objects.create(name="lvl7", is_active=False)
        self.lvl4 = Level4Up.objects.create(name="lvl4", level7=self.lvl7)
        self.lvl3 = Level3Up.objects.create(name="lvl3", level4=self.lvl4, is_active=True)

        self.assertFalse(self.lvl7.is_active)
        self.assertEqual(self.lvl7.save_count, 1)


# Public Mixin IsUpActive TestCase #####################################################################################

class PublicModelUpIsActiveMixinTest(TestCase):
    """
       Making sure public class inherit from private
      +-----------+    +----------+
      | Level A   +--->+ Level B  +
      +-----------+    +----------+
       """

    def test_public_upwardmixin(self):
        self.lvlb = LevelBUp.objects.create(name="lvlb", is_active=False)
        self.lvla = LevelAUp.objects.create(name="lvla", is_active=False, levelb=self.lvlb)

        self.lvla.is_active = True
        self.lvla.save()

        self.assertTrue(self.lvlb.is_active)
