# Import ###############################################################################################################
from django.db import models

from pixelforest_drf.utils.models_mixins import _UpIsActiveMixin, UpIsActiveMixin, CountSaveModelMixin


# Models Test for _UpIsActiveMixin #####################################################################################
class AbstractLevel(CountSaveModelMixin):
    is_active = models.BooleanField(default=False, null=False, blank=True)
    name = models.CharField(max_length=256, null=False, blank=False)

    class Meta:
        abstract = True


class Level8Up(AbstractLevel, _UpIsActiveMixin):
    pass


class Level7Up(AbstractLevel, _UpIsActiveMixin):
    pass


class Level6Up(CountSaveModelMixin):
    level8 = models.ForeignKey(to=Level8Up, null=True, blank=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=256, null=False, blank=False)


class Level5Up(AbstractLevel, _UpIsActiveMixin):
    level6 = models.ForeignKey(to=Level6Up, null=True, blank=True, on_delete=models.PROTECT)


class Level4Up(_UpIsActiveMixin, CountSaveModelMixin):
    level7 = models.ForeignKey(to=Level7Up, null=True, blank=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=256, null=False, blank=False)


class Level3Up(AbstractLevel, _UpIsActiveMixin):
    level4 = models.ForeignKey(to=Level4Up, null=True, blank=True, on_delete=models.SET_NULL)


class Level2Up(AbstractLevel, _UpIsActiveMixin):
    level3 = models.ForeignKey(to=Level3Up, null=True, blank=True, on_delete=models.PROTECT)
    level4 = models.ForeignKey(to=Level4Up, null=True, blank=True, on_delete=models.PROTECT)


class Level1Up(AbstractLevel, _UpIsActiveMixin):
    level2 = models.ForeignKey(to=Level2Up, null=True, blank=True, on_delete=models.PROTECT)
    level5 = models.ForeignKey(to=Level5Up, null=True, blank=True, on_delete=models.SET_NULL)


class LevelBUp(UpIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)


class LevelAUp(UpIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)
    levelb = models.ForeignKey(to=LevelBUp, null=True, blank=True, on_delete=models.PROTECT)
