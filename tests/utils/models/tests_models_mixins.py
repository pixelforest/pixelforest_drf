# Import ###############################################################################################################
from django.db import models

from pixelforest_drf.utils.models_mixins import AbrModelMixin, NotModifiableFieldsModelMixin, CountSaveModelMixin


# Tests for AbrModelMixin ##############################################################################################
class AbrModelWithNameField(AbrModelMixin, models.Model):
    name = models.CharField(max_length=100)


class AbrModel(AbrModelMixin, models.Model):
    pass


# Tests for CountSaveModelMixin ########################################################################################
class SaveCounterWithName(CountSaveModelMixin):
    name = models.CharField(max_length=100)


class BadSaveImplementation(CountSaveModelMixin):
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        """ Bad implementation that will call .save() multiple times """
        if not self._state.adding:
            super().save(*args, **kwargs)
        super().save(*args, **kwargs)


class ModelWithNotModifiableFields(NotModifiableFieldsModelMixin):
    name = models.CharField(max_length=10)
    description = models.TextField()

    not_modifiable_fields = ['name']

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
