# Imports ##############################################################################################################
from django.db import models
from pixelforest_drf.utils.models_mixins import _UpAndDownIsActiveMixin, UpAndDownIsActiveMixin


# Models Test for _UpAndDownIsActiveMixin ##############################################################################
class AbstractLevel(_UpAndDownIsActiveMixin):
    is_active = models.BooleanField(default=False, null=False, blank=True)
    name = models.CharField(max_length=256, null=False, blank=False)

    class Meta:
        abstract = True


class Level2UpAndDown(AbstractLevel):
    pass


class Level1UpAndDown(AbstractLevel):
    level2 = models.ForeignKey(to=Level2UpAndDown, null=True, blank=True, on_delete=models.PROTECT)


class LevelBUpAndDown(UpAndDownIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)


class LevelAUpAndDown(UpAndDownIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)
    levelb = models.ForeignKey(to=LevelBUpAndDown, null=False, blank=False, on_delete=models.PROTECT)