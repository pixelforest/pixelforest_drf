# Imports ##############################################################################################################
from django.db import models
from pixelforest_drf.utils.models_mixins import _DownIsActiveMixin, DownIsActiveMixin


# Models Test for _DownIsActiveMixin ###################################################################################

class AbstractLevel(_DownIsActiveMixin):
    is_active = models.BooleanField(default=False, null=False, blank=True)
    name = models.CharField(max_length=256, null=False, blank=False)

    class Meta:
        abstract = True


class Level8down(AbstractLevel):
    pass


class Level7down(AbstractLevel):
    pass


class Level6down(models.Model):
    level8 = models.ForeignKey(to=Level8down, null=True, blank=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=256, null=False, blank=False)
    is_active = models.BooleanField(default=False, null=False, blank=True)


class Level5down(AbstractLevel):
    level6 = models.ForeignKey(to=Level6down, null=True, blank=True, on_delete=models.PROTECT)


class Level4down(_DownIsActiveMixin):
    level7 = models.ForeignKey(to=Level7down, null=True, blank=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=256, null=False, blank=False)


class Level3down(AbstractLevel):
    level4 = models.ForeignKey(to=Level4down, null=True, blank=True, on_delete=models.SET_NULL)


class Level2down(AbstractLevel):
    level3 = models.ForeignKey(to=Level3down, null=True, blank=True, on_delete=models.PROTECT)
    level4 = models.ForeignKey(to=Level4down, null=True, blank=True, on_delete=models.SET_NULL)


class Level1down(AbstractLevel):
    level2 = models.ForeignKey(to=Level2down, null=True, blank=True, on_delete=models.PROTECT)
    level5 = models.ForeignKey(to=Level5down, null=True, blank=True, on_delete=models.SET_NULL)


class IsActiveWithMixin(AbstractLevel):
    pass


class IsActiveWithoutMixin(models.Model):
    is_active = models.BooleanField(default=False, null=False, blank=True)
    name = models.CharField(max_length=256, null=False, blank=False)
    withmixin = models.ForeignKey(to=IsActiveWithMixin, null=False, blank=False, on_delete=models.PROTECT)

class LevelBdown(DownIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)


class LevelAdown(DownIsActiveMixin):
    name = models.CharField(max_length=256, null=False, blank=False)
    levelb = models.ForeignKey(to=LevelBdown, null=True, blank=True, on_delete=models.PROTECT)

