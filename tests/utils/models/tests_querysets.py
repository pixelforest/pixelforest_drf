# Import ###############################################################################################################
from django.db import models

from pixelforest_drf.utils.querysets import ActiveQuerySet


# Tests for ActiveQuerySet #############################################################################################
class IsActiveModel(models.Model):
    """ Simple model with only is_active field """
    name = models.CharField(max_length=64)
    is_active = models.BooleanField()

    objects = ActiveQuerySet.as_manager()
