# Imports ##############################################################################################################
from django.db import models


# Tests ################################################################################################################
class BasicModel(models.Model):
    text = models.CharField(max_length=100, verbose_name="Text comes here", help_text="Text description.")
