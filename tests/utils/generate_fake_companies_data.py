# Import ###############################################################################################################
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

from pixelforest_drf.companies.models import CompanyGroup, Company, Subsidiary, Service


# Fake data generation #################################################################################################

# Company Groups
acme = CompanyGroup.objects.get_or_create(name="ACME Group", abbreviation="ACME",
                                          is_active=True)[0]
globex = CompanyGroup.objects.get_or_create(name='Globex Corporation', abbreviation="GLOBEX",
                                            is_active=False)[0]

# Companies
anvil = Company.objects.get_or_create(name="ACME Falling Anvils", abbreviation="Anvil",
                                      is_active=True, company_group=acme)[0]
box = Company.objects.get_or_create(name="ACME Mail-Order Box Kit", abbreviation="Box",
                                    is_active=True, company_group=acme)[0]
etb = Company.objects.get_or_create(name="ACME Explosive Tennis Balls",
                                    abbreviation="ETB", company_group=acme)[0]
nucl = Company.objects.get_or_create(name="Globex Nuclear Plant", abbreviation="NuPl",
                                     is_active=True, company_group=globex)[0]
pf = Company.objects.get_or_create(name="PixelForest", abbreviation="PF", is_active=True)[0]

# Subisdiaries and Services
for company in [anvil, box, etb, nucl]:
    for sub_spec in ([" - Europe", "EU"], [" - United States", "US"]):
        subsidiary = Subsidiary.objects.get_or_create(name=company.name + sub_spec[0], abbreviation=sub_spec[1],
                                                      is_active=company.is_active, company=company)[0]
        for ser_spec in (["Marketing Team", "Market"], ["Media Team", "Media"],
                         ["Dev Team", "Dev"], ["Accounting", "$"]):
            Service.objects.get_or_create(name=ser_spec[0], abbreviation=ser_spec[1],
                                          is_active=subsidiary.is_active, subsidiary=subsidiary)

pf_paris = Subsidiary.objects.get_or_create(name="PixeForest Paris", abbreviation="PAR",
                                            company=pf, is_active=pf.is_active)[0]
pf_bdx = Subsidiary.objects.get_or_create(name="PixeForest Bordeaux", abbreviation="BDX",
                                          company=pf, is_active=pf.is_active)[0]

Service.objects.get_or_create(name="Accounting", abbreviation="$", subsidiary=pf_paris, is_active=True)
Service.objects.get_or_create(name="Accounting", abbreviation="$", subsidiary=pf_bdx, is_active=True)
Service.objects.get_or_create(name="Marketing Team", abbreviation="Market", subsidiary=pf_bdx, is_active=True)
Service.objects.get_or_create(name="Dev Team", abbreviation="Dev", subsidiary=pf_paris, is_active=True)
