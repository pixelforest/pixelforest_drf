from django.apps import AppConfig


class utilsConfig(AppConfig):
    name = 'pixelforest_drf.utils'
