# Imports ##############################################################################################################
import base64

from django.contrib.auth.models import Permission
from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework import HTTP_HEADER_ENCODING, authentication, generics, status, serializers
from rest_framework.test import APIRequestFactory

from pixelforest_drf.companies.models import Service
from pixelforest_drf.rest.permissions import FullDjangoModelPermissions

from tests.utils.models import BasicModel

factory = APIRequestFactory()
User = get_user_model()


# Create a basic serializer ############################################################################################
class BasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = BasicModel
        fields = '__all__'


# Create basic views ###################################################################################################
class BasicView(generics.ListCreateAPIView):
    queryset = BasicModel.objects.all()
    serializer_class = BasicSerializer
    authentication_classes = [authentication.BasicAuthentication]
    permission_classes = [FullDjangoModelPermissions]


basic_view = BasicView.as_view()


# Method too authenticate a user #######################################################################################
def basic_auth_header(username, password):
    credentials = ('%s:%s' % (username, password))
    base64_credentials = base64.b64encode(credentials.encode(HTTP_HEADER_ENCODING)).decode(HTTP_HEADER_ENCODING)
    return 'Basic %s' % base64_credentials


# Tests ################################################################################################################
class ModelPermissionsIntegrationTests(TestCase):
    """
    Those tests are only ran on the read/view permission, as FullDjangoModelPermission is a child of the
    DjangoModelPermission already tested by DRF
    """
    @classmethod
    def setUpClass(cls):
        # Get a service
        service = Service.superuser_default()

        # Create a user with and a user without access to the resource
        cls.ok_user = User.objects.create_user(email='ok@example.com',
                                               first_name="O",
                                               last_name="K",
                                               service=service,
                                               password='password')

        cls.ok_user.user_permissions.set([
            Permission.objects.get(codename='view_basicmodel'),
        ])

        cls.ko_user = User.objects.create_user(email='ko@example.com',
                                               first_name="K",
                                               last_name="O",
                                               service=service,
                                               password='password')

        cls.ok_credentials = basic_auth_header(cls.ok_user.email, 'password')
        cls.ko_credentials = basic_auth_header(cls.ko_user.email, 'password')

        # Create one simple object
        BasicModel(text='foo').save()

    @classmethod
    def tearDownClass(cls):
        User.objects.filter(pk__in=[cls.ok_user.pk, cls.ko_user.pk]).delete()

    def test_has_read_permissions_on_list(self):
        request = factory.get('/', HTTP_AUTHORIZATION=self.ok_credentials)
        response = basic_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_has_read_permissions_on_details(self):
        request = factory.get('/1', HTTP_AUTHORIZATION=self.ok_credentials)
        response = basic_view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_does_not_have_read_permissions_on_list(self):
        request = factory.get('/', HTTP_AUTHORIZATION=self.ko_credentials)
        response = basic_view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_does_not_have_read_permissions_on_details(self):
        request = factory.get('/1', HTTP_AUTHORIZATION=self.ko_credentials)
        response = basic_view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
