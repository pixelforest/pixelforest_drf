# Imports ##############################################################################################################

from django.contrib.auth import get_user_model
from django.urls import reverse

# DRF Testing
from rest_framework import status
from rest_framework.test import APITestCase

# Models
from pixelforest_drf.users.models import PFUser
from pixelforest_drf.companies.models import Service, Subsidiary, Company

User = get_user_model()


# Testing ##############################################################################################################
class CheckPermissionsAPITestCase(APITestCase):
    """
    We use this class ensure that the following specs are respected:

    Can access the api:
    * A superuser
    * An active user

    Can't access the API:
    * A non logged in user
    * An inactive user
    """

    @classmethod
    def setUpClass(cls):
        cls.acme = Company.objects.create(name="Acme")
        cls.acme_france = Subsidiary.objects.create(name="Acme France", company=cls.acme)
        cls.dev = Service.objects.create(name="Dev Team", subsidiary=cls.acme_france)
        # Create super user ############################################################################################
        cls.superuser = PFUser.objects.create_superuser(
            email="superuser@test.fr",
            password="12345AJ?",
            first_name='active superuser',
            last_name='Testing',
            phone='+33611223344',
        )
        # Create inactive user #########################################################################################
        cls.inactive_user = PFUser.objects.create_user(
            email="inactive_user@test.fr",
            password="12345AJ?",
            first_name='Inactive_user',
            last_name='Testing',
            phone='+33611223344',
            service=cls.dev,
            is_active=False
        )
        # Create active user ###########################################################################################
        cls.active_user = PFUser.objects.create_user(
            email="active_user@test.fr",
            password="12345AJ?",
            first_name='active_user',
            last_name='Testing',
            phone='+33611223344',
            service=cls.dev,
        )

    @classmethod
    def tearDownClass(cls):
        User.objects.filter(pk__in=[cls.superuser.pk, cls.inactive_user.pk, cls.active_user.pk]).delete()
        Service.objects.filter(pk=cls.dev.pk).delete()
        Subsidiary.objects.filter(pk=cls.acme_france.pk).delete()
        Company.objects.filter(pk=cls.acme.pk).delete()

    # Superuser ########################################################################################################
    def test_super_user_can_access_user_list(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:user-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_user_me(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:me-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_countries(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:countries-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_sub_region(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:sub_region-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_region(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:region-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_service(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:service-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_subsidiary(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:subsidiary-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_company(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:company-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_super_user_can_access_company_group(self):
        self.client.login(username="superuser@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:company_group-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # User #############################################################################################################

    def test_inactive_user_cant_log_in(self):
        self.assertFalse(self.client.login(username="inactive_user@test.fr", password="12345AJ?"))

    def test_active_user_can_access_me(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:me-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_active_user_cant_access_user_list(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:user-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_countries(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:countries-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_sub_region(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:sub_region-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_region(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:region-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_service(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:service-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_subsidiary(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:subsidiary-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_company(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:company-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_active_user_cant_access_company_group(self):
        self.client.login(username="active_user@test.fr", password="12345AJ?")
        url = reverse('pixelforest_drf:company_group-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
