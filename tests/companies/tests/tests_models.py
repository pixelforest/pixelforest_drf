# Imports ##############################################################################################################
from django.db.models.constraints import UniqueConstraint
from django.test import SimpleTestCase

from pixelforest_drf.companies.models import CompanyGroup, Company, Subsidiary, Service
from pixelforest_drf.utils.querysets import DownIsActiveMixinManager

# Tests ################################################################################################################

class CompanyGroupTestCase(SimpleTestCase):
    def test_uniqueness(self):
        # constraint on company
        self.assertEqual(len([c for c in CompanyGroup._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('name',)]), 1)
        # constraint on abbreviation
        self.assertEqual(len([c for c in CompanyGroup._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('abbreviation',)]), 1)


class CompanyTestCase(SimpleTestCase):
    def test_uniqueness_constraints(self):
        # constraint on company group + name
        self.assertEqual(len([c for c in Company._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('company_group', 'name')]), 1)
        # constraint on company group + abbreviation
        self.assertEqual(len([c for c in Company._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('company_group', 'abbreviation')]), 1)


class SubsidiaryTestCase(SimpleTestCase):
    def test_uniqueness_constraints(self):
        # constraint on company + name
        self.assertEqual(len([c for c in Subsidiary._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('company', 'name')]), 1)
        # constraint on company + abbreviation
        self.assertEqual(len([c for c in Subsidiary._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('company', 'abbreviation')]), 1)


class ServiceTestCase(SimpleTestCase):
    def test_uniqueness_constraints(self):
        # constraint on company + name
        self.assertEqual(len([c for c in Service._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('subsidiary', 'name')]), 1)
        # constraint on company + abbreviation
        self.assertEqual(len([c for c in Service._meta.constraints
                              if isinstance(c, UniqueConstraint) and c.fields == ('subsidiary', 'abbreviation')]), 1)


class MixinInheritTesTCase(SimpleTestCase):
    """ Tests that the IsActiveMixin affect queryset objects"""

    def test_Group_mixin(self):
        self.assertEqual(type(CompanyGroup.objects), DownIsActiveMixinManager, "UpAndDownIsActive Mixin is Missing in "
                                                                               "Group")

    def test_Company_mixin(self):
        self.assertEqual(type(Company.objects), DownIsActiveMixinManager, "UpAndDownIsActive Mixin is Missing in "
                                                                          "Company")

    def test_Subsidiary_mixin(self):
        self.assertEqual(type(Subsidiary.objects), DownIsActiveMixinManager, "UpAndDownIsActive Mixin is Missing in "
                                                                             "Subsidiary")

    def test_Service_mixin(self):
        self.assertEqual(type(Service.objects), DownIsActiveMixinManager, "UpAndDownIsActive Mixin is Missing in "
                                                                          "Service")
