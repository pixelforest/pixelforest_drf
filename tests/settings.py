# Imports ##############################################################################################################
import os

# Django base settings for testing #####################################################################################

DEBUG = True
USE_TZ = True

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "p*@kl)5m7+5+u2!c*8d)xc9*+llyhfrf-vij+(1_(#zkzxv7*q"

# Use a PostgreSQL DB
DATABASES = {
    'default': {
      'ENGINE': 'django.db.backends.postgresql',
      'HOST': 'localhost',
      'PORT': '5432',
      'NAME': 'pixelforest_drf',
      'USER': 'django',
      'PASSWORD': 'pwd',
    }
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Change the URL confs for the one in tests
ROOT_URLCONF = "tests.urls"

# When adding another subpackage, add it here for unit testing
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    "pixelforest_drf.countries",
    "pixelforest_drf.companies",
    "pixelforest_drf.users",
    "tests.utils",  # Utils is here only for mixin testing, and do not have to be installed in a real app
    'rest_framework',
    'phonenumber_field',
]

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
]

MEDIA_ROOT = './tests/media'
MEDIA_URL = '/media/'

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "tests/static"),
]

# Change the test running app and add verbosity
# TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
# NOSE_ARGS = ['--with-spec', '--spec-color']

SITE_ID = 1

AUTH_USER_MODEL = "users.PFUser"
