# Imports ##############################################################################################################
from freezegun import freeze_time
from datetime import datetime, timedelta
from pytz import UTC

from django.core.exceptions import ValidationError
from django.test import SimpleTestCase, TestCase
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient

from pixelforest_drf.companies.models import Service, Subsidiary, Company
from pixelforest_drf.countries.models import Country, SubRegion, Region
from pixelforest_drf.users.models import PFUser
from pixelforest_drf.users.managers import UserAndDownIsActiveManager

User = get_user_model()


# Tests ################################################################################################################

class TestUserConfig(SimpleTestCase):
    def test_user_is_correctly_setup(self):
        """ Verify that we have the PFUser as a User in Django settings """
        self.assertEqual(User, PFUser)


class BaseTestUser(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Get one object per Countries
        cls.fr = Country.objects.get(name="France")
        cls.sub = SubRegion.objects.create(name="Random SubRegion")
        cls.reg = Region.objects.create(name="Random Region")

        # Get one object per Companies

        cls.acme = Company.objects.create(name="Acme")
        cls.acme_france = Subsidiary.objects.create(name="Acme France", company=cls.acme)
        cls.dev = Service.objects.create(name="Dev Team", subsidiary=cls.acme_france)
        cls.dev1 = Service.objects.create(name="Media Team", subsidiary=cls.acme_france)
        cls.dev2 = Service.objects.create(name="Business Team", subsidiary=cls.acme_france)

        # Create the base user infos
        cls.basepassword = "A123ajsidl!"
        cls.baseinfos = {
            'first_name': "Thomas",
            'last_name': "Pesquet",
            'password': cls.basepassword,
            'service': cls.dev,
        }


class TestPFUserCountries(BaseTestUser):
    """ We test directly PFUser For the Mixin functionality because it's a pain to define another User Model """
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_global_user_creation(self):
        User.objects.create(email="tp@pixelforest.io", **self.baseinfos)

    def test_one_country_object_user_creation_work(self):
        User.objects.create(email="tp1@pixelforest.io", **self.baseinfos, country=self.fr)

    def test_two_country_object_user_creation_fail(self):
        with self.assertRaisesMessage(ValidationError, "{'__all__': ['The user tp@pixelforest.io must be linked to one "
                                                       "Countries object (but is linked to country, sub_region)']}"):
            User.objects.create(email="tp@pixelforest.io", **self.baseinfos, country=self.fr, sub_region=self.sub)

    def test_linked_company_object_user_creation_fail(self):
        """Test if a user can not be linked to a Service (he mustn't) """
        with self.assertRaisesMessage(ValidationError, "{'service': ['This field cannot be null.']}"):
            User.objects.create(email="tp@pixelforest.io", first_name="Justine", last_name="Dupont",
                                password=self.basepassword)


# Tests ################################################################################################################

class UserTestCase(BaseTestUser):
    """ Test related to the base django user functionalities """
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Create the client
        cls.client = APIClient()

        # Create a user
        cls.user = User.objects.create_user(email="tp@pixelforest.io", **cls.baseinfos)

    def test_get_full_name(self):
        """ Test that we get the last name and first name """
        self.assertEqual(self.user.get_full_name(), "Pesquet Thomas")

    def test_get_short_name(self):
        """ Test that we get the first name of the user """
        self.assertEqual(self.user.get_short_name(), "Thomas")

    def test_last_login_is_set_after_login(self):
        """ Test that last_login is set to None after creation and set to the timestamp of the login after a login """
        # Before first login, is None
        self.assertIsNone(self.user.last_login)

        # After a login, it is set to the timestamp
        time = datetime(year=2019, month=1, day=1, tzinfo=UTC)
        with freeze_time(time):
            self.assertTrue(self.client.login(email=self.user.email, password=self.basepassword))
            self.user = User.objects.get(pk=self.user.pk)
            self.assertEqual(self.user.last_login, time)
            self.client.logout()

        # After a second login, it is set to the new timestamp
        time += timedelta(hours=6)
        with freeze_time(time):
            self.assertTrue(self.client.login(email=self.user.email, password=self.basepassword))
            user = User.objects.get(pk=self.user.pk)
            self.assertEqual(user.last_login, time)
            self.client.logout()


class MixinInheritTesTCase(SimpleTestCase):
    """ Tests that the IsActiveMixin affect queryset objects"""

    def test_mixin_User_Inherit(self):
        self.assertEqual(type(PFUser.objects), UserAndDownIsActiveManager , "Mixin is Missing in PfUser Models")
