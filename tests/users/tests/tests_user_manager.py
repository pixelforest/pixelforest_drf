# Imports ##############################################################################################################

from django.test import TestCase
from django.contrib.auth import get_user_model

from pixelforest_drf.companies.models import Service, Subsidiary, Company
User = get_user_model()


# Tests ################################################################################################################

class UserTestCase(TestCase):
    """ Verify that the manager used for our custom user will correctly create (super)users """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.acme = Company.objects.create(name="Acme")
        cls.acme_france = Subsidiary.objects.create(name="Acme France", company=cls.acme)
        cls.dev = Service.objects.create(name="Dev Team", subsidiary=cls.acme_france)

    def test_superuser_creation_with_defaults(self):
        superuser = User.objects.create_superuser(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            phone='+33611223344',
        )
        self.assertEqual(superuser.is_superuser, True)
        self.assertEqual(superuser.is_staff, True)

    def test_superuser_creation_without_defaults(self):
        superuser = User.objects.create_superuser(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            is_superuser=True,
            is_staff=True,
            phone='+33611223344',
        )
        self.assertEqual(superuser.is_superuser, True)
        self.assertEqual(superuser.is_staff, True)

    def test_superuser_creation_fail_with_is_superuser_false(self):
        with self.assertRaisesMessage(ValueError, 'Superuser must have is_superuser=True'):
            User.objects.create_superuser(
                email="superuser@test.fr",
                password=None,
                first_name='Superuser',
                last_name='Testing',
                is_superuser=False,
                phone='+33611223344',
            )

    def test_user_creation_with_is_superuser_true_create_superuser(self):
        superuser = User.objects.create_user(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            is_superuser=True,
            is_staff=True,
            phone='+33611223344',
            service=self.dev
        )
        self.assertEqual(superuser.is_superuser, True)
        self.assertEqual(superuser.is_staff, True)

    def test_user_creation_with_defaults(self):
        user = User.objects.create_user(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            phone='+33611223344',
            service=self.dev,
        )
        self.assertEqual(user.is_superuser, False)
        self.assertEqual(user.is_staff, False)

    def test_user_creation_without_defaults(self):
        user = User.objects.create_user(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            is_superuser=False,
            is_staff=False,
            phone='+33611223344',
            service=self.dev,
        )
        self.assertEqual(user.is_superuser, False)
        self.assertEqual(user.is_staff, False)

    def test_super_user_creation_service_default_name_is_pf_data_team(self):
        superuser = User.objects.create_superuser(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            is_superuser=True,
            is_staff=True,
            phone='+33611223344',
        )
        self.assertEqual(superuser.is_superuser, True)
        self.assertEqual(superuser.is_staff, True)
        self.assertEqual(superuser.service.name, "Data Team")

    def test_super_user_creation_custom_service(self):
        superuser = User.objects.create_superuser(
            email="superuser@test.fr",
            password=None,
            first_name='Superuser',
            last_name='Testing',
            is_superuser=True,
            is_staff=True,
            phone='+33611223344',
            service=self.dev
        )